from Base.object3D_class import Object3D
from Base.matrix_class import Matrix
from numpy.linalg import inv


class Camera(Object3D):

    def __init__(self, angleOfView=60, aspectRatio=1, near=0.1, far=1000):
        super().__init__()
        self.projectionMatrix = Matrix.makePerspective(angleOfView,
                                                       aspectRatio,
                                                       near, far)
        self.viewMatrix = Matrix.makeIdentity()

    def updateViewMatrix(self):
        """ view Matrix = inverse of transform matrix """
        self.viewMatrix = inv( self.getWorldMatrix())

    def setPerspective(self, angleOfView=50, aspectRatio=1, near=0.1, far=1000):
        self.projectionMatrix = Matrix.makePerspective(angleOfView,
                                                       aspectRatio, near, far)

    def setOrthographic(self, left=-1, right=1, bottom=-1, top=1, near=-1, far=1):
        self.projectionMatrix = Matrix.makeOrthographic(left, right,
                                                        bottom, top,
                                                        near, far)
