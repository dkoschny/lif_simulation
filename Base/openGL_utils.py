from OpenGL.GL import *


class OpenGL_utils(object):
    """  static methods load/compile/open
     GL shaders and link to create GPU programs """

    @staticmethod
    def initializeShader(shaderCode, shaderType):
        # specify GLSL version
        shaderCode = "#version 330 \n" + shaderCode
        # create empty shader object and return ref value
        shaderRef = glCreateShader(shaderType)
        # store source code in shader
        glShaderSource(shaderRef, shaderCode)
        # compile source code stored in the shader
        glCompileShader(shaderRef)
        # query whether compilation was successful
        compile_success = glGetShaderiv(shaderRef, GL_COMPILE_STATUS)
        if not compile_success:
            # retrieve error message
            error_message = glGetShaderInfoLog(shaderRef)
            # free memory used for shader program
            glDeleteShader(shaderRef)
            # convert byte str to char str
            error_message = "\n" + error_message.decode("utf-8")
            # raise exception to halt program and print error msg
            raise Exception(error_message)
        # compilation was successful
        return shaderRef

    @staticmethod
    def initializeProgram(vertexShaderCode, fragmentShaderCode):
        # compile shaders and store references
        vertexShaderRef = OpenGL_utils.initializeShader(vertexShaderCode, GL_VERTEX_SHADER)
        fragmentShaderRef = OpenGL_utils.initializeShader(fragmentShaderCode, GL_FRAGMENT_SHADER)
        # create program object
        programRef = glCreateProgram()

        # attach previously compiled shaders
        glAttachShader(programRef, vertexShaderRef)
        glAttachShader(programRef, fragmentShaderRef)

        # link vertex shader to fragment shader
        glLinkProgram(programRef)
        # query if linking was successful
        linkSuccess = glGetProgramiv(programRef, GL_LINK_STATUS)
        if not linkSuccess:
            # retrieve error message
            errorMessage = glGetProgramInfoLog(programRef)
            # free memory used to store program
            glDeleteProgram(programRef)
            # convert byte str to char str
            errorMessage = "\n" + errorMessage.decode("utf-8")
            # raise exception, halt program, print error message
            raise Exception(errorMessage)
        # linking was successful, return program reference
        return programRef

    @staticmethod
    def printSystemInfo():
        print("=======================================================")
        print(" Vendor: " + glGetString(GL_VENDOR).decode('utf-8'))
        print(" Renderer: " + glGetString(GL_RENDERER).decode('utf-8'))
        print(" OpenGL Version supported: " + glGetString(GL_VERSION).decode('utf-8'))
        print(" GLSL version supported: " + glGetString(GL_SHADING_LANGUAGE_VERSION).decode('utf-8'))
        print("=======================================================")
