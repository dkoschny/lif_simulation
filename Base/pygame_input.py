import pygame


class Input(object):
    """ input module for LIF simulation """

    def __init__(self):
        # has the user quit?
        self.quit = False

        # lists to store key states
        # down, up: discrete events, lasts for one iteration
        # pressed: continuous event, between up/down events
        self.keyDownList = []
        self.keyUpList = []
        self.keyPressedList = []

    def update(self):
        # iterate over all input events (keyboard/mouse) since last time checked
        # reset discrete key states
        self.keyDownList = []
        self.keyUpList = []

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.quit = True

            # check for key events, get name of key, append/remove from list
            if event.type == pygame.KEYDOWN:
                keyName = pygame.key.name(event.key)
                self.keyDownList.append(keyName)
                self.keyPressedList.append(keyName)

            if event.type == pygame.KEYUP:
                keyName = pygame.key.name(event.key)
                self.keyUpList.append(keyName)
                self.keyPressedList.remove(keyName)

    def isKeyDown(self, keyName):
        """ check if key is down """
        return keyName in self.keyDownList

    def isKeyUp(self, keyName):
        """ check if key is up """
        return keyName in self.keyUpList

    def isKeyPressed(self, keyName):
        """ check if key is currently being pressed """
        return keyName in self.keyPressedList
