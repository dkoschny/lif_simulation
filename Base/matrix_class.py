import numpy as np
from numpy.linalg import norm
from math import sin, cos, tan, pi


class Matrix(object):

    @staticmethod
    def makeIdentity():
        """ identity matrix """
        return np.array([[1, 0, 0, 0],
                         [0, 1, 0, 0],
                         [0, 0, 1, 0],
                         [0, 0, 0, 1]]).astype(float)

    @staticmethod
    def makeTranslation(x, y, z):
        """ translation along axes """
        return np.array([[1, 0, 0, x],
                         [0, 1, 0, y],
                         [0, 0, 1, z],
                         [0, 0, 0, 1]]).astype(float)

    @staticmethod
    def makeRotationX(angle):
        """ rotation around X-axis """
        c = cos(angle)
        s = sin(angle)

        return np.array([[1, 0, 0, 0],
                         [0, c, -s, 0],
                         [0, s, c, 0],
                         [0, 0, 0, 1]]).astype(float)

    @staticmethod
    def makeRotationY(angle):
        """ rotation around Y-axis """
        c = cos(angle)
        s = sin(angle)

        return np.array([[c, 0, s, 0],
                         [0, 1, 0, 0],
                         [-s, 0, c, 0],
                         [0, 0, 0, 1]]).astype(float)

    @staticmethod
    def makeRotationZ(angle):
        """ rotation around Z-axis """
        c = cos(angle)
        s = sin(angle)

        return np.array([[c, -s, 0, 0],
                         [s, c, 0, 0],
                         [0, 0, 1, 0],
                         [0, 0, 0, 1]]).astype(float)

    @staticmethod
    def makeScale(s):
        """ scale coordinates by s """

        return np.array([[s, 0, 0, 0],
                         [0, s, 0, 0],
                         [0, 0, s, 0],
                         [0, 0, 0, 1]]).astype(float)

    @staticmethod
    def makeLookAt(position, target):
        worldUp = [0, 1, 0]
        forward = np.subtract(target, position)
        right = np.cross(forward, worldUp)

        # if forward and worldUp vectors are parallel -> right vector=0
        # fix by perturbing worldUp by a small amount
        if norm(right) < 0.001:
            offset = np.array([0.001, 0, 0])
            right = np.cross(forward, worldUp + offset)

        up = np.cross(right, forward)

        # normalizing all vectors
        forward = np.divide(forward, norm(forward))
        right = np.divide(right, norm(right))
        up = np.divide(up, norm(up))

        # print(np.array([[right[0], up[0],
        #                  -forward[0], position[0]],
        #                 [right[1], up[1],
        #                  -forward[1], position[1]],
        #                 [right[2], up[2],
        #                  -forward[2], position[2]],
        #                 [0, 0, 0, 1]]
        #                ))

        return np.array([[right[0], up[0],
                          -forward[0], position[0]],
                         [right[1], up[1],
                          -forward[1], position[1]],
                         [right[2], up[2],
                          -forward[2], position[2]],
                         [0, 0, 0, 1]]
                        )

    @staticmethod
    def makePerspective(angleOfView=60, aspectRatio=1, near=0.1, far=100):
        """ change perspective of virtual camera """

        # convert deg into rad
        a = angleOfView * pi / 180
        # distance to adjusted viewing plane
        d = 1.0 / tan(a / 2)
        # aspect ratio
        r = aspectRatio
        b = (far + near) / (near - far)
        c = 2 * far * near / (near - far)

        return np.array([[d / r, 0, 0, 0],
                         [0, d, 0, 0],
                         [0, 0, b, c],
                         [0, 0, -1, 0]]).astype(float)

    @staticmethod
    def makeOrthographic(left=-1, right=1, bottom=-1, top=1,
                         near=-1, far=1):
        return np.array([[2/(right-left), 0, 0, -(right+left)/(right-left)],
                         [0, 2/(top-bottom), 0, -(top+bottom)/(top-bottom)],
                         [0, 0, -2/(far-near), -(far+near)/(far-near)],
                         [0, 0, 0, 1]])
