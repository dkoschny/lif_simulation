import pygame
import sys
import configparser
from Base.pygame_input import Input


class Base(object):

    def __init__(self, config_file):
        """ initialize all pygame modules """
        pygame.init()

        # setup config parser
        config_file_name = config_file
        config = configparser.RawConfigParser()
        config.read(config_file_name)

        # window width, height
        WIDTH = config["camera settings"].getint("window_width")
        HEIGHT = config["camera settings"].getint("window_height")
        screen_size = (WIDTH, HEIGHT)

        # indicate rendering options
        display_flags = pygame.DOUBLEBUF | pygame.OPENGL

        # initialize buffers to perform anti aliasing
        pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLEBUFFERS, 1)
        pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLEBUFFERS, 4)

        # use a core OpenGL profile for cross platform compatibility
        pygame.display.gl_set_attribute(pygame.GL_CONTEXT_PROFILE_MASK,
                                        pygame.GL_CONTEXT_PROFILE_CORE)

        # create and display window
        self.screen = pygame.display.set_mode(screen_size, display_flags)
        pygame.display.set_caption("Lunar Impact Simulator")

        # determines if main loop is running
        self.running = True

        # manage time related data/operations
        self.clock = pygame.time.Clock()

        # manage user input
        self.input = Input()

    def initialize(self):
        """ implement by extending class """
        pass

    def update(self):
        """ implement by extending class """
        pass

    def run(self):
        """ function to run the main loop """
        self.initialize()

        # main loop
        while self.running:
            # process input
            self.input.update()

            if self.input.quit:
                self.running = False

            # update
            self.update()

            # render
            # display image on screen
            pygame.display.flip()
            # pause if necessary for 60 fps
            self.clock.tick(60)

        # shutdown
        pygame.quit()
        sys.exit()
