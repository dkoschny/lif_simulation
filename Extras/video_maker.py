
import os
import cv2


def make_video(frame_path, video_path, video_name, output_type):
    frame_number = 0
    frame_array = []

    for i in os.listdir(frame_path):
        image = frame_path + f'frame{frame_number}.{output_type}'
        frame_array.append(image)
        frame_number += 1

    cv2_fourcc = cv2.VideoWriter_fourcc(*'mp4v')

    frame = cv2.imread(frame_array[0])
    size = list(frame.shape)
    del size[2]
    size.reverse()

    video = cv2.VideoWriter(video_path + video_name + '.mp4', cv2_fourcc, 60, size)

    for i in range(len(frame_array)):
        video.write(cv2.imread(frame_array[i]))

    video.release()
