import csv


def schedule_file_reader(input_path):
    time, lat, long, brightness, duration = [], [], [], [], []
    i = 0
    with open(input_path, 'r') as file:
        input_file = csv.reader(file)
        for row in input_file:
            if row:
                i += 1
                if i > 1:
                    time.append(float(row[0]))
                    lat.append(float(row[1]))
                    long.append(float(row[2]))
                    brightness.append(float(row[3]))
                    duration.append(float(row[4]))
    return time, lat, long, brightness, duration
