from Base.scene_class import Scene
from Base.camera import Camera
from Base.mesh_class import Mesh
from Base.render_target import RenderTarget
from Geometry.geometry_class import Geometry


class Postprocessor(object):
    """ postprocessor class, create geometric object to work with effect materials """

    def __init__(self, renderer, scene, camera, finalRenderTarget=None):

        self.renderer = renderer

        self.sceneList = [scene]
        self.cameraList = [camera]
        self.renderTargetList = [finalRenderTarget]
        self.finalRenderTarget = finalRenderTarget
        self.orthoCamera = Camera()
        self.orthoCamera.setOrthographic()  # aligned with clip space by default

        # generate a rectangle already aligned with clip space;
        # no matrix transformations will be applied
        self.rectangleGeo = Geometry()
        P0, P1, P2, P3 = [-1, -1], [1, -1], [-1, 1], [1, 1]
        T0, T1, T2, T3 = [0, 0], [1, 0], [0, 1], [1, 1]
        positionData = [P0, P1, P3, P0, P3, P2]
        uvData = [T0, T1, T3, T0, T3, T2]
        self.rectangleGeo.addAttribute("vec2", "vertex_position", positionData)
        self.rectangleGeo.addAttribute("vec2", "vertex_UV", uvData)
        self.rectangleGeo.countVertices()

    def addEffect(self, effect):
        """ add post processing effects via the effect parameter.
            effect is essentially material containing simple shaders
            to work with post processor object  """
        
        postScene = Scene()
        resolution = self.renderer.windowSize
        target = RenderTarget(resolution)

        # change the previous entry in the render target list to
        # this newly created render target
        self.renderTargetList[-1] = target

        # the effect in this render pass will use the texture that was written
        # in the previous render pass
        effect.uniforms["my_texture"].data[0] = target.my_texture.textureRef

        mesh = Mesh(self.rectangleGeo, effect)
        postScene.add(mesh)
        self.sceneList.append(postScene)
        self.cameraList.append(self.orthoCamera)
        self.renderTargetList.append(self.finalRenderTarget)

    def render(self):
        """ render scene with postprocessing effects """

        passes = len(self.sceneList)
        for n in range(passes):
            scene = self.sceneList[n]
            camera = self.cameraList[n]
            target = self.renderTargetList[n]
            self.renderer.render(scene, camera, renderTarget=target)
