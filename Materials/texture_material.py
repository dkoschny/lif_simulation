from materials.material_class import Material
from OpenGL.GL import *

# changed texture2D to texture! might cause issues?


class TextureMaterial(Material):

    def __init__(self, mytexture, properties={}):
        vertexShaderCode = """
        uniform mat4 projectionMatrix;
        uniform mat4 viewMatrix;
        uniform mat4 modelMatrix;
        in vec3 vertexPosition;
        in vec2 vertexUV;
        out vec2 UV;
        
        void main()
        {
            gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1);
            UV = vertexUV;
        }
        """

        fragmentShaderCode = """
        uniform vec3 baseColor;
        uniform sampler2D mytexture;
        in vec2 UV;
        out vec4 fragColor;
        
        void main()
        {
            vec4 color = vec4(baseColor, 1);
            fragColor = color * texture(mytexture, UV);
        }
        """
        super().__init__(vertexShaderCode, fragmentShaderCode)

        self.addUniform("vec3", "baseColor", [1, 1, 1])
        self.addUniform("sampler2D", "mytexture", [mytexture.textureRef, 1])
        self.locateUniforms()

        # render both sides?
        self.settings["doubleSide"] = True
        # render triangles as wireframe?
        self.settings["wireframe"] = False
        # line thickness for wireframe
        self.settings["lineWidth"] = 1
        self.settings["drawStyle"] = GL_TRIANGLES

        self.setProperties(properties)

    def updateRenderSettings(self):

        if self.settings["doubleSide"]:
            glDisable(GL_CULL_FACE)
        else:
            glEnable(GL_CULL_FACE)

        if self.settings["wireframe"]:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
        else:
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

        glLineWidth(self.settings["lineWidth"])
