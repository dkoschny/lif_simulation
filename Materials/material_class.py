from Base.openGL_utils import OpenGL_utils
from Base.uniform import Uniform
from OpenGL.GL import *


class Material(object):

    def __init__(self, vertexShaderCode, fragmentShaderCode):
        self.programRef = OpenGL_utils.initializeProgram(
            vertexShaderCode, fragmentShaderCode)

        # store uniform objects / dictionary -> need key to store each obj under
        self.uniforms = {}

        # standard uniform objects (matrices)
        self.uniforms["modelMatrix"] = Uniform("mat4", None)
        self.uniforms["viewMatrix"] = Uniform("mat4", None)
        self.uniforms["projectionMatrix"] = Uniform("mat4", None)

        # store OpenGl render settings
        self.settings = {}
        self.settings["drawStyle"] = GL_TRIANGLES  # changed from "None" to GL_TRIANGLES because of ctype render error

    def locateUniforms(self):
        """ initialize all uniform variable references """
        for variableName, uniformObject in self.uniforms.items():
            uniformObject.locateVariable(self.programRef, variableName)

    def addUniform(self, dataType, variableName, data):
        """ add new uniforms """
        self.uniforms[variableName] = Uniform(dataType, data)

    def updateRenderSettings(self):
        """ configure OpenGL render settings"""
        pass

    def setProperties(self, properties={}):
        """ convenience method for setting multiple properties:
            uniform and render setting values   """
        for name, data in properties.items():
            # update uniforms
            if name in self.uniforms.keys():
                self.uniforms[name].data = data
            # update render settings
            elif name in self.settings.keys():
                self.settings[name] = data
            # unknown property
            else:
                raise Exception("Material has no property: " + name)
