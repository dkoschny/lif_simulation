# base imports
import os
import os.path
import pygame.image
import configparser
from OpenGL.GL import *
from Extras.video_maker import make_video
# additional packages needed: pygame, NumPy, PyOpenGL, cv2

from Base.pygame_window import Base
from Base.renderer import Renderer
from Base.render_target import RenderTarget
from Base.scene_class import Scene
from Base.camera import Camera
from Base.mesh_class import Mesh
from Base.texture_class import Texture

# geometry and material imports
from Geometry.sphere_geometry import SphereGeometry
from Geometry.rectangle_geometry import RectangleGeometry

# lighting imports
from Materials.lambert_material import LambertMaterial
from Materials.surface_basic_material import SurfaceBasicMaterial
from Lighting.ambient_light import AmbientLight
from Lighting.directional_light import DirectionalLight

# Extra imports
from Extras.input_file_reader import schedule_file_reader
from Extras.movement_rig import MovementRig
from Extras.postprocessor import Postprocessor
from Extras.get_moon_geometry import get_moon_geometry

# effect imports
from Effects.vignette_effect import VignetteEffect
from Effects.atmosphere_effect import AtmosphereEffect
from Effects.glow_effect import AdditiveBlendEffect
from Effects.horizontal_blur_effect import HorizontalBlurEffect
from Effects.vertical_blur_effect import VerticalBlurEffect
from Effects.cloud_effect import CloudEffect
from Effects.horizontal_gauss_effect import HorizontalGaussEffect
from Effects.vertical_gauss_effect import VerticalGaussEffect

# math imports
from math import sin, cos, tan, exp, pi


class LifSimulation(Base):
    """ Simulates lunar impact flashes
        on a spherical model of the moon with adjustable number of flashes,
        simulation duration, brightness and observation date"""

    def __init__(self, config_file):
        super().__init__(config_file)

        # set up configparser
        try:
            config_file_name = config_file
            self.config = configparser.RawConfigParser()
            self.config.read(config_file_name)
        except FileNotFoundError:
            print("config file not found!")

        # file paths
        try:
            self.base_path = self.config["path definitions"].get("base_path")
        except FileNotFoundError:
            print("Base path does not exist")
        try:
            self.frame_path = self.config["path definitions"].get("frame_path")
            self.video_path = self.config["path definitions"].get("video_path")
        except FileNotFoundError:
            print("video or frame path does not exist")

        # import user settings
        # video and camera settings
        self.video_mode = self.config["video mode"].getboolean("video_mode")
        self.video_name = self.config["video mode"].get("video_name")
        self.output_type = self.config["video mode"].get("output_type")
        self.delete_files = self.config["video mode"].getboolean("delete_files")
        self.field_of_view = self.config["camera settings"].getfloat("field_of_view")
        self.camera_rotation_x = self.config["camera settings"].getfloat("camera_rotation_x")
        self.camera_rotation_y = self.config["camera settings"].getfloat("camera_rotation_y")
        self.camera_rotation_z = self.config["camera settings"].getfloat("camera_rotation_z")
        self.window_width = self.config["camera settings"].getint("window_width")
        self.window_height = self.config["camera settings"].getint("window_height")

        # observation settings
        self.simulation_length = self.config["observation settings"].getint("simulation_length")
        observation_start = self.config["observation settings"].get("observation_start")
        observation_end = self.config["observation settings"].get("observation_end")
        observation_location = self.config["observation settings"].get("observation_location")
        step_size = self.config["observation settings"].get("step_size")

        # LIF settings
        self.lif_size = self.config["lif settings"].getint("lif_size")
        self.exp_decay = self.config["lif settings"].getboolean("exp_decay")

        # import schedule file
        self.schedule_file_data = schedule_file_reader(self.base_path + 'schedule_file.CSV')

        # import user post processing parameters
        self.ambient_strength = self.config["light intensities"].getfloat("ambient_intensity")
        sun_light_strength = self.config["light intensities"].getfloat("sun_light_intensity")
        bump_strength = self.config["light intensities"].getfloat("bump_strength")

        self.use_atmosphere_effect = self.config["post processing settings"].getboolean("use_atmosphere_effect")
        amplitude = self.config["post processing settings"].getfloat("amplitude")

        self.use_blur_effect = self.config["post processing settings"].getboolean("use_blur_effect")
        blur_radius = self.config["post processing settings"].getint("blur_radius")

        self.use_clouds_effect = self.config["post processing settings"].getboolean("use_clouds_effect")

        self.use_vignette_effect = self.config["post processing settings"].getboolean("use_vignette_effect")
        dim_start = self.config["post processing settings"].getfloat("dim_start")
        dim_end = self.config["post processing settings"].getfloat("dim_end")

        self.use_glow_effect = self.config["post processing settings"].getboolean("use_glow_effect")
        self.glow_blur_radius = self.config["post processing settings"].getint("blend_blur_radius")
        self.original_strength = self.config["post processing settings"].getfloat("original_strength")
        self.glow_strength = self.config["post processing settings"].getfloat("glow_strength")
        self.blend_material_color = self.config["post processing settings"].getfloat("blend_material_color")

        # initialize LIF/time indices
        self.time = 0
        self.time_index = 0
        self.obs_timer = 0
        self.counter = 0
        self.sun_light_direction = []
        self.ambient_color_array = []

        self.lif_timer = 0
        self.lif_index = 0
        self.lif_count = 1
        self.lif_bool = False

        # get moon data using get_moon routine
        self.moon_geometry_data = []
        self.moon_geometry_data = get_moon_geometry(observation_start, observation_end,
                                                    step_size, observation_location)
        self.obs_time = self.moon_geometry_data[0][0]
        self.obs_sub_lat = float(self.moon_geometry_data[1][0]) * pi / 180
        self.obs_sub_long = float(self.moon_geometry_data[2][0]) * pi / 180
        self.sol_sub_lat = float(self.moon_geometry_data[3][0]) * pi / 180
        self.sol_sub_long = float(self.moon_geometry_data[4][0]) * pi / 180
        self.moon_distance = float(self.moon_geometry_data[5][0]) / 1000

        # import schedule file data
        self.max_lif = len(self.schedule_file_data[0])
        self.mag = 1.0
        self.longitude = 0
        self.latitude = 0
        self.duration = self.schedule_file_data[4][self.lif_index]
        self.color = [self.mag, self.mag, self.mag]
        self.sphere_coords = (0, 0, 0)

        # set up lighting
        self.ambient_color = self.ambient_strength
        self.directional = \
            DirectionalLight(color=[sun_light_strength, sun_light_strength, sun_light_strength],
                             direction=[0, 0, -1])
        self.ambient = AmbientLight()

        # set up scene, renderer and camera
        self.scene = Scene()
        self.glow_scene = Scene()
        self.renderer = Renderer()  # renderer is "drawStyle" atm not GL_TRIANGLES MIGHT CAUSE ctype error
        self.camera = Camera(aspectRatio=self.window_width / self.window_height,
                             angleOfView=self.field_of_view,
                             near=0.01 * self.moon_distance,
                             far=self.moon_distance * 2)  # 1920 / 1080 fullscreen
        self.rig = MovementRig()

        # Calculate LIF size to be in orders of Pixel
        self.window_height_opengl_coords = \
            tan((self.field_of_view * pi / 180) / 2) * self.moon_distance * 2
        self.window_width_opengl_coords = \
            self.window_width / self.window_height * self.window_height_opengl_coords
        self.lif_height = self.lif_size * self.window_height_opengl_coords / self.window_height
        self.lif_width = self.lif_size * self.window_width_opengl_coords / self.window_width

        # initialize LIF geometry and material
        self.lif_material = SurfaceBasicMaterial()
        self.lif_geometry = RectangleGeometry(width=self.lif_width, height=self.lif_height)
        self.lif = Mesh(self.lif_geometry, self.lif_material)

        # set up moon
        self.moon_geometry = SphereGeometry(radius=1.737)
        moon_texture = Texture(self.base_path + 'Textures/moon_texture.jpg')
        bump_texture = Texture(self.base_path + 'Textures/moon_bump_map.jpg')
        self.moon_material = LambertMaterial(mytexture=moon_texture,
                                             bumpTexture=bump_texture,
                                             bump_strength=bump_strength)
        self.glow_material = LambertMaterial(mytexture=None)
        self.moon = Mesh(self.moon_geometry, self.moon_material)
        self.moon.rotateY(0)

        # set up post processor and effects
        self.combo_pass = Postprocessor(self.renderer, self.scene, self.camera)
        self.glow_target = RenderTarget(resolution=[self.window_width, self.window_height])
        self.glow_pass = Postprocessor(self.renderer, self.glow_scene,
                                       self.camera, self.glow_target)
        self.atmosphere_effect = AtmosphereEffect(amplitude=amplitude)
        self.cloud_effect = CloudEffect()
        self.horizontal_gauss = HorizontalGaussEffect(texture_size=[self.window_width,
                                                                    self.window_height],
                                                      blur_radius=blur_radius)
        self.vertical_gauss = VerticalGaussEffect(texture_size=[self.window_width,
                                                                self.window_height],
                                                  blur_radius=blur_radius)
        self.vignette_effect = VignetteEffect(dim_start=dim_start,
                                              dim_end=dim_end, dim_color=[0, 0, 0])

    def initialize(self):
        """ set up 3D environment including the objects in it
            (moon model, virtual camera etc.) """

        # add different light types to the scene
        self.ambient_color = \
            self.ambient_strength * cos(self.sol_sub_long + pi) + self.ambient_strength
        self.ambient.color = [self.ambient_color, self.ambient_color, self.ambient_color]
        self.scene.add(self.ambient)
        self.directional.rotateX(self.sol_sub_lat, localCoord=False)
        self.directional.rotateY(self.sol_sub_long, localCoord=False)
        self.scene.add(self.directional)

        # set Moon position
        self.moon.setPosition([0, 0, 0])
        self.scene.add(self.moon)

        # add movement rig to camera and apply observation, and rotation data
        self.rig.setPosition([0, 0, self.moon_distance])
        self.rig.add(self.camera)
        self.rig.rotateX(self.obs_sub_lat, localCoord=False)
        self.rig.rotateY(self.obs_sub_long, localCoord=False)
        self.rig.rotateX(self.camera_rotation_x * pi / 180, localCoord=True)
        self.rig.rotateY(self.camera_rotation_y * pi / 180, localCoord=True)
        self.rig.rotateZ(self.camera_rotation_z * pi / 180, localCoord=True)

        self.scene.add(self.rig)

        # glow scene settings
        self.glow_material.uniforms["baseColor"].data = \
            [self.blend_material_color, self.blend_material_color, self.blend_material_color]
        glow_sphere = Mesh(self.moon_geometry, self.glow_material)
        glow_sphere.transform = self.moon.transform
        self.glow_scene.add(glow_sphere)
        self.glow_scene.add(self.directional)
        # glow post processing settings
        self.glow_pass.addEffect(HorizontalBlurEffect(blur_radius=self.glow_blur_radius))
        self.glow_pass.addEffect(VerticalBlurEffect(blur_radius=self.glow_blur_radius))

        # add post processing effects
        if self.use_atmosphere_effect:
            self.combo_pass.addEffect(self.atmosphere_effect)
        if self.use_blur_effect:
            self.combo_pass.addEffect(self.horizontal_gauss)
            self.combo_pass.addEffect(self.vertical_gauss)
        if self.use_clouds_effect:
            self.combo_pass.addEffect(self.cloud_effect)
        if self.use_vignette_effect:
            self.combo_pass.addEffect(self.vignette_effect)
        if self.use_glow_effect:
            self.combo_pass.addEffect(AdditiveBlendEffect
                                      (self.glow_target.my_texture,
                                       original_strength=self.original_strength,
                                       blend_strength=self.glow_strength))

        if self.delete_files:
            file_number = len(os.listdir(self.frame_path))
            for filename in os.listdir(self.frame_path):
                file_path = os.path.join(self.frame_path, filename)
                try:
                    if os.path.isfile(file_path) or os.path.islink(file_path):
                        os.unlink(file_path)
                except Exception as e:
                    print('Failed to delete %s. Reason: %s' % (file_path, e))
            print(f"{file_number} frames have been deleted")
        print(self.schedule_file_data[3][self.lif_index])

    def update(self):

        """ simulation loop. LIFs are added and removed from the scene """
        self.lif_timer += 1 / 60
        self.obs_timer += 1 / 60
        self.time += 1 / 60

        # Save every frame to "frames" folder. Possibly move to make video function
        if self.video_mode is True and self.counter % 1 == 0:
            screen = pygame.display.get_surface()
            size = screen.get_size()
            buffer = glReadPixels(0, 0, *size, GL_RGBA, GL_UNSIGNED_BYTE)
            screen_surf = pygame.image.fromstring(buffer, size, "RGBA")
            final_frame = pygame.transform.flip(screen_surf, False, True)
            pygame.image.save(final_frame,
                              self.frame_path + f'/frame{self.counter}.{self.output_type}')
        self.counter += 1

        self.rig.update(self.input, 1 / 60)
        self.atmosphere_effect.uniforms["time"].data += 10 / 60
        self.cloud_effect.uniforms["time"].data -= 1 / 3000
        self.glow_pass.render()
        self.combo_pass.render()

        if self.obs_timer >= self.simulation_length / len(self.moon_geometry_data[0]) and \
                self.time_index < len(self.moon_geometry_data[0]) - 1:
            self.time_index += 1

            # apply updated moon geometry data
            self.obs_time = self.moon_geometry_data[0][self.time_index]
            self.obs_sub_lat = float(self.moon_geometry_data[1][self.time_index]) * pi / 180
            self.obs_sub_long = float(self.moon_geometry_data[2][self.time_index]) * pi / 180
            self.sol_sub_lat = float(self.moon_geometry_data[3][self.time_index]) * pi / 180
            self.sol_sub_long = float(self.moon_geometry_data[4][self.time_index]) * pi / 180
            self.moon_distance = float(self.moon_geometry_data[5][self.time_index]) / 1000

            # reset camera, rig and sunlight
            self.rig.setPosition([0, 0, self.moon_distance])
            self.rig.lookAt([0, 0, 0])
            self.directional.setDirection([0, 0, -1])

            # rotate directional light and camera according to sub_solar and sub_observer points
            # and adjust ambient light
            self.directional.rotateX(self.sol_sub_lat, localCoord=False)
            self.directional.rotateY(self.sol_sub_long, localCoord=False)
            self.ambient_color = \
                self.ambient_strength * cos(self.sol_sub_long + pi) + self.ambient_strength
            self.ambient.color = [self.ambient_color, self.ambient_color, self.ambient_color]
            self.rig.rotateX(self.obs_sub_lat, localCoord=False)
            self.rig.rotateY(self.obs_sub_long, localCoord=False)

            # rotate the camera in its local coordinate system according to user settings again
            self.rig.rotateX(self.camera_rotation_x * pi / 180, localCoord=True)
            self.rig.rotateY(self.camera_rotation_y * pi / 180, localCoord=True)
            self.rig.rotateZ(self.camera_rotation_z * pi / 180, localCoord=True)

            self.obs_timer = 0

        elif self.time_index == len(self.moon_geometry_data[0]) - 1:
            print(" Last observation time reached!")
            self.time_index += 1

        elif self.time >= self.simulation_length:
            print(f" Time limit ({self.simulation_length} s) reached!"
                  f" Terminating simulation loop")
            self.running = False

        # exponentially falling LIF brightness
        if self.lif_bool is True and self.exp_decay is True:
            self.mag = self.schedule_file_data[3][self.lif_index - 1] * exp(-self.lif_timer / self.duration)
            self.lif_material.uniforms["baseColor"].data = \
                [self.mag, self.mag, self.mag]

        # add/remove lunar impact flashes to the scene if conditions are met
        if self.lif_count <= self.max_lif:
            if self.lif_timer >= self.schedule_file_data[0][self.lif_index] and \
                    self.lif_bool is False:
                self.latitude = self.schedule_file_data[1][self.lif_index] * pi / 180
                self.longitude = self.schedule_file_data[2][self.lif_index] * pi / 180
                self.mag = self.schedule_file_data[3][self.lif_index]
                self.duration = self.schedule_file_data[4][self.lif_index]  # 0.033-0.4

                # light source distance to lunar surface is 0.01 at 1.747
                self.sphere_coords = [1.747 * sin(self.longitude) * cos(self.latitude),
                                      1.747 * sin(self.latitude),
                                      1.747 * cos(self.longitude) * cos(self.latitude)]

                self.lif_height = \
                    self.lif_size * self.window_height_opengl_coords / self.window_height
                self.lif_width = \
                    self.lif_size * self.window_width_opengl_coords / self.window_width
                self.lif_geometry = \
                    RectangleGeometry(width=self.lif_width, height=self.lif_height)
                self.lif_material = \
                    SurfaceBasicMaterial({"baseColor": [self.mag, self.mag, self.mag]})
                self.lif = Mesh(self.lif_geometry, self.lif_material)

                self.lif.setPosition([self.sphere_coords[0],
                                      self.sphere_coords[1],
                                      self.sphere_coords[2]])
                self.lif.lookAt(self.camera.getWorldPosition())
                self.scene.add(self.lif)
                self.lif_bool = True
                self.lif_count += 1
                print(f" Event data:    "
                      f"    latitude: {'%.3f' % (self.latitude * 180 / pi)}" +
                      f"    longitude: {'%.3f' % (self.longitude * 180 / pi)}" +
                      f"    RGB value: {self.mag * 255}"
                      f"    duration: {'%.3f' % self.duration} s"
                      f"    time: {'%.2f' % self.time} s")
                self.lif_index += 1
                self.lif_timer = 0

            elif self.lif_timer >= self.duration and self.lif_bool is True:

                self.scene.remove(self.lif)
                self.lif_bool = False
                self.lif_timer = 0
        # try except block ended here
        elif self.lif_count > self.max_lif:
            print("Maximum LIF number reached! Terminating simulation loop.")
            self.running = False
            pass

        if self.running is False and self.video_mode is True:
            make_video(self.frame_path, self.video_path, self.video_name, self.output_type)
            print(f" Video has been successfully saved as {self.video_name} in {self.video_path}")


LifSimulation('config_file.ini').run()
