from Materials.material_class import Material


class VignetteEffect(Material):

    def __init__(self, dim_start=0.4, dim_end=1.0, dim_color=[0, 0, 0]):
        vertexShaderCode = """
        in vec2 vertex_position;
        in vec2 vertex_UV;
        out vec2 UV;

        void main()
        {
            gl_Position = vec4(vertex_position, 0.0, 1.0);
            UV = vertex_UV;
        }
        """

        fragmentShaderCode = """
        in vec2 UV;
        uniform sampler2D my_texture;
        uniform float dim_start;
        uniform float dim_end;
        uniform vec3 dim_color;
        out vec4 frag_color;

        void main()
        {
            vec4 color = texture(my_texture, UV);
            
            // calculate position in clip space from UV coordinates
            vec2 position = 2 * UV - vec2(1, 1);
            
            // calculate distance d from center, which affects brightness
            float d = length(position);
             
            // calculate brightness b factor:
            // when d = dim_start, b = 1; when d = dim_end, b = 0.
            float b = (d - dim_end) / (dim_start - dim_end);
            
            //prevent oversaturation
            b = clamp(b, 0, 1);
            
            // mix the texture color and dim color
            frag_color = vec4(b * color.rgb + (1-b) * dim_color, 1);
            
        }
        """

        super().__init__(vertexShaderCode, fragmentShaderCode)
        # texture is rendered without any change (pass-through shader)
        self.addUniform("sampler2D", "my_texture", [None, 1])
        self.addUniform("float", "dim_start", dim_start)
        self.addUniform("float", "dim_end", dim_end)
        self.addUniform("vec3", "dim_color", dim_color)
        self.locateUniforms()
