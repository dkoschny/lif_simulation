from Materials.material_class import Material


class HorizontalBlurEffect(Material):

    def __init__(self, texture_size=[512, 512], blur_radius=20):
        vertexShaderCode = """
        in vec2 vertex_position;
        in vec2 vertex_UV;
        out vec2 UV;

        void main()
        {
            gl_Position = vec4(vertex_position, 0.0, 1.0);
            UV = vertex_UV;
        }
        """

        fragmentShaderCode = """
        in vec2 UV;
        uniform sampler2D my_texture;
        uniform vec2 texture_size;
        uniform int blur_radius;
        out vec4 frag_color;

        void main()
        {
            vec2 pixel_to_texture_coords = 1/texture_size;
            vec4 average_color = vec4(0,0,0,0);
            
            for (int offset_X = -blur_radius; offset_X <= blur_radius; offset_X++)
                {
                float weight = blur_radius - abs(offset_X) + 1;
                vec2 offset_UV = vec2(offset_X, 0) * pixel_to_texture_coords;
                average_color += texture(my_texture, UV + offset_UV) * weight;
                }
                
            average_color /= average_color.a;
            frag_color = average_color;
        }
        """

        super().__init__(vertexShaderCode, fragmentShaderCode)
        # texture is rendered without any change (pass-through shader)
        self.addUniform("sampler2D", "my_texture", [None, 1])
        self.addUniform("vec2", "texture_size", texture_size)
        self.addUniform("int", "blur_radius", blur_radius)
        self.locateUniforms()
