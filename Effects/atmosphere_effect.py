from Materials.material_class import Material


class AtmosphereEffect(Material):

    def __init__(self, amplitude=0.0005):
        vertexShaderCode = """
        in vec2 vertex_position;
        in vec2 vertex_UV;
        out vec2 UV;

        void main()
        {
            gl_Position = vec4(vertex_position, 0.0, 1.0);
            UV = vertex_UV;
        }
        """

        fragmentShaderCode = """
    
        in vec2 UV;
        uniform sampler2D my_texture;
        uniform float time;
        uniform float amplitude;
        out vec4 frag_color;

        void main()
        {   
        
            vec2 shift_UV = UV + vec2(0, amplitude * sin(80 * UV.x + time));
            frag_color = texture(my_texture, shift_UV);
        }
        """

        super().__init__(vertexShaderCode, fragmentShaderCode)
        # texture is rendered without any change (pass-through shader)
        self.addUniform("sampler2D", "my_texture", [None, 1])
        self.addUniform("float", "amplitude", amplitude)
        self.addUniform("float", "time", 0.0)
        self.locateUniforms()
