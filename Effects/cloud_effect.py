from Materials.material_class import Material


class CloudEffect(Material):

    def __init__(self):
        vertexShaderCode = """
        in vec2 vertex_position;
        in vec2 vertex_UV;
        out vec2 UV;

        void main()
        {
            gl_Position = vec4(vertex_position, 0.0, 1.0);
            UV = vertex_UV;
        }
        """

        fragmentShaderCode = """
        
        // generate random values in [0,1]
        float random(vec2 UV)
        {
            return fract(235711.0 * sin(14.337 * UV.x + 42.418 * UV.y));
        }
        
        float box_random(vec2 UV, float scale)
        {
            vec2 i_scale_UV = floor(scale * UV);
            return random(i_scale_UV);
        }
        
        float smooth_random(vec2 UV, float scale)
        {
            vec2 i_scale_UV = floor(scale * UV);
            vec2 f_scale_UV = fract(scale * UV);
            float a = random(i_scale_UV);
            float b = random(round(i_scale_UV + vec2(1, 0)));
            float c = random(round(i_scale_UV + vec2(0, 1)));
            float d = random(round(i_scale_UV + vec2(1, 1)));
            return mix( mix(a, b, f_scale_UV.x),
                        mix(c, d, f_scale_UV.x),
                        f_scale_UV.y);
        }
        
        // add smooth random values at different scales
        // weighted (amplitudes) so that sum is approximately 1.0
        
        float fractal_random(vec2 UV, float scale)
        {
            float value = 0.0;
            float amplitude = 0.5;
            
            for (int i = 0; i<6; i++)
            {
                value += amplitude * smooth_random(UV, scale);
                scale *= 2.0;
                amplitude *= 0.5;
            }
            
            return value;
        }
        
        in vec2 UV;
        uniform sampler2D my_texture;
        uniform float time;
        out vec4 frag_color;
        
        void main()
        {
            vec2 shift_UV = UV + vec2(time, 0);
            float r = fractal_random(shift_UV, 5);
            vec4 color1 = texture(my_texture, UV);
            vec4 color2 = vec4(0.05, 0.05, 0.1, 1);
            frag_color = mix(color1, color2, r);
        }
        """

        super().__init__(vertexShaderCode, fragmentShaderCode)
        # texture is rendered without any change (pass-through shader)
        self.addUniform("sampler2D", "my_texture", [None, 1])
        self.addUniform("float","time", 0)
        self.locateUniforms()
