from Materials.material_class import Material


class AdditiveBlendEffect(Material):

    def __init__(self, blend_texture=None, original_strength=1.0,
                 blend_strength=1.0):
        vertexShaderCode = """
        in vec2 vertex_position;
        in vec2 vertex_UV;
        out vec2 UV;

        void main()
        {
            gl_Position = vec4(vertex_position, 0.0, 1.0);
            UV = vertex_UV;
        }
        """

        fragmentShaderCode = """
        in vec2 UV;
        uniform sampler2D my_texture;
        uniform sampler2D blend_texture;
        uniform float original_strength;
        uniform float blend_strength;
        uniform float time;
        out vec4 frag_color;

        void main()
        {
            vec4 original_color = texture(my_texture, UV);
            vec4 blend_color = texture(blend_texture, UV);
            vec4 color = original_strength * original_color + blend_strength * max(0, cos(time)) * blend_color;
            frag_color = color;
        }
        """

        super().__init__(vertexShaderCode, fragmentShaderCode)
        # texture is rendered without any change (pass-through shader)
        self.addUniform("sampler2D", "my_texture", [None, 1])
        self.addUniform("sampler2D", "blend_texture", [blend_texture.textureRef, 2])
        self.addUniform("float", "original_strength", original_strength)
        self.addUniform("float", "blend_strength", blend_strength)
        self.addUniform("float", "time", 0.0)
        self.locateUniforms()
