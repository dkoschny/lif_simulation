import numpy as np

from Base.attributes import Attribute


class Geometry(object):
    def __init__(self):

        # dictionary to store attribute objects
        self.attributes = {}

        # number of vertices to be rendered by gl draw array
        self.vertexCount = None

    def addAttribute(self, dataType, variableName, data):
        self.attributes[variableName] = Attribute(dataType, data)

    def countVertices(self):
        # the number of vertices is the length of any
        # attribute objects array of data
        attrib = list( self.attributes.values())[0]
        self.vertexCount = len(attrib.data)

    def applyMatrix(self, matrix, variableName="vertexPosition"):
        """ transform the data in an attribute using a matrix"""

        # transform position data
        oldPositionData = self.attributes[variableName].data
        newPositionData = []

        for oldPos in oldPositionData:
            # avoid changing list references
            newPos = oldPos.copy()
            # add homogeneous fourth coordinate
            newPos.append(1)
            # multiply by matrix
            newPos = matrix @ newPos
            # remove homogeneous coordinate
            newPos = list(newPos[0:3])
            # add to new data list
            newPositionData.append(newPos)

        self.attributes[variableName].data = newPositionData

        # extract the rotation submatrix
        rotationMatrix = np.array([ matrix[0][0:3],
                                    matrix[1][0:3],
                                    matrix[2][0:3]])

        # transform normal vector data
        oldVertexNormalData = self.attributes["vertexNormal"].data
        newVertexNormalData = []
        for oldNormal in oldVertexNormalData:
            newNormal = oldNormal.copy()
            newNormal = rotationMatrix @ newNormal
            newVertexNormalData.append(newNormal)

        self.attributes["vertexNormal"].data = newVertexNormalData

        oldFaceNormalData = self.attributes["faceNormal"].data
        newFaceNormalData = []
        for oldNormal in oldFaceNormalData:
            newNormal = oldNormal.copy()
            newNormal = rotationMatrix @ newNormal
            newFaceNormalData.append(newNormal)

        self.attributes["faceNormal"].data = newFaceNormalData

        # new data must be uploaded
        self.attributes[variableName].uploadData()

    def merge(self, otherGeometry):
        """ merge data from attributes of other geometry into this object;
            requires both geometries to have attributes with same names """
        for variableName, attributeObject in self.attributes.items():
            attributeObject.data += otherGeometry.attributes[variableName].data
            attributeObject.uploadData()

        # update vertices number
        self.countVertices()
