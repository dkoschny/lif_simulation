from Geometry.geometry_class import Geometry
import numpy as np


class ParametricGeometry(Geometry):
    """ meant to be extended, need to supply function """

    def __init__(self, uStart, uEnd, uResolution,
                 vStart, vEnd, vResolution, S):
        super().__init__()
        # generate set of points on the function
        # width of segment/number of segments = segment size

        deltaU = (uEnd - uStart) / uResolution
        deltaV = (vEnd - vStart) / vResolution

        positions = []
        uvs = []
        vertexNormals = []

        # calculate content of the uvs list
        for uIndex in range(uResolution + 1):
            vArray = []
            for vIndex in range(vResolution + 1):
                u = uStart + uIndex * deltaU
                v = vStart + vIndex * deltaV
                vArray.append(S(u, v))
            positions.append(vArray)

        for uIndex in range(uResolution + 1):
            vArray = []
            for vIndex in range(vResolution + 1):
                u = uIndex / uResolution
                v = vIndex / vResolution
                vArray.append([u, v])
            uvs.append(vArray)

        def calcNormal(P0, P1, P2):
            """ calculate normal vector from P0, P1, P2,
                create list of normal vectors at position of each vertex """
            v1 = np.array(P1) - np.array(P0)
            v2 = np.array(P2) - np.array(P0)
            normal = np.cross(v1, v2)
            normal = normal / np.linalg.norm(normal)  # normalize
            return normal

        for uIndex in range(uResolution + 1):
            vArray = []
            for vIndex in range(vResolution + 1):
                u = uStart + uIndex * deltaU
                v = vStart + vIndex * deltaV
                h = 0.0001
                P0 = S(u, v)
                P1 = S(u + h, v)
                P2 = S(u, v + h)
                normalVector = calcNormal(P0, P1, P2)
                vArray.append(normalVector)
            vertexNormals.append(vArray)

        # store vertex data
        positionData = []
        colorData = []
        uvData = []
        vertexNormalData = []
        faceNormalData = []

        # default vertex colors
        C1, C2, C3 = [1, 0, 0], [0, 1, 0], [0, 0, 1]
        C4, C5, C6 = [0, 1, 1], [1, 0, 1], [1, 1, 0]

        # group vertex data into triangles
        for xIndex in range(uResolution):
            for yInDex in range(vResolution):
                # position data
                pA = positions[xIndex + 0][yInDex + 0]
                pB = positions[xIndex + 1][yInDex + 0]
                pC = positions[xIndex + 1][yInDex + 1]
                pD = positions[xIndex + 0][yInDex + 1]

                positionData += [pA, pB, pC, pA, pC, pD]
                colorData += [C1, C2, C3, C4, C5, C6]

                # uv coordinates
                uvA = uvs[xIndex + 0][yInDex + 0]
                uvB = uvs[xIndex + 1][yInDex + 0]
                uvC = uvs[xIndex + 1][yInDex + 1]
                uvD = uvs[xIndex + 0][yInDex + 1]

                uvData += [uvA, uvB, uvC, uvA, uvC, uvD]

                # vertex normal vectors
                nA = vertexNormals[xIndex + 0][yInDex + 0]
                nB = vertexNormals[xIndex + 1][yInDex + 0]
                nD = vertexNormals[xIndex + 0][yInDex + 1]
                nC = vertexNormals[xIndex + 1][yInDex + 1]

                vertexNormalData += [nA, nB, nC, nA, nC, nD]

                # face normal vectors
                fn0 = calcNormal(pA, pB, pC)
                fn1 = calcNormal(pA, pC, pD)
                faceNormalData += [fn0, fn0, fn0, fn1, fn1, fn1]

        self.addAttribute("vec3", "vertexPosition", positionData)
        self.addAttribute("vec3", "vertexColor", colorData)
        self.addAttribute("vec2", "vertexUV", uvData)
        self.addAttribute("vec3", "vertexNormal", vertexNormalData)
        self.addAttribute("vec3", "faceNormal", faceNormalData)

        self.countVertices()
